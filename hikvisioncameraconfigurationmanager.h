#ifndef HIKVISIONCAMERACONFIGURATIONMANAGER_H
#define HIKVISIONCAMERACONFIGURATIONMANAGER_H

#include <ctime>

#include "gstreamerthreadworker.h"

class HikvisionCameraConfigurationManager
{
public:
    HikvisionCameraConfigurationManager();

    bool setCurrentTimeToCamera(const GstreamerThreadWorker::CameraType& cameraType);
};

#endif  // HIKVISIONCAMERACONFIGURATIONMANAGER_H
