#ifndef SETTINGS_H
#define SETTINGS_H

#include <QString>

void createFileTest();
QString restoreLeftCameraAddress();
QString restoreRightCameraAddress();
QString restoreDefaultVideoFolder();

#endif  // SETTINGS_H
