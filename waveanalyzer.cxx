#include "waveanalyzer.hpp"

#include <iostream>
#include <cassert>
#include <fstream>

void WaveAnalyzer::analyze()
{
    while (true) {
        switch (_state) {
        case noExistsBaseLine: {
            if (getEnoughData()) {
                if (findBaseline()) {
                    _state = noNegFront;
                }
                else {
                    _buffers.erase(_buffers.begin(), _buffers.end());
                    _timeBuffers.erase(_timeBuffers.begin(), _timeBuffers.end());
                    return;
                }
            }
            else {
                return;
            }
        }
        case noNegFront: {
            if (getEnoughData()) {
                if (findNegativeFront()) {
                    _state = decodeWave;
                }
                else {
                    _buffers.erase(_buffers.begin(), _buffers.end());
                    _timeBuffers.erase(_timeBuffers.begin(), _timeBuffers.end());
                    _state = noExistsBaseLine;
                    return;
                }
            }
            else {
                return;
            }
        }
        case decodeWave: {
            if (getEnoughData()) {
                unsigned int res = 0;
                SignalsBuffer buff;
                buff.insert(buff.begin(), _buffers.begin(), (_buffers.begin() + WAVE_WIDTH_I));
                if (decodeBuffer(buff, res)) {
                    GstClockTime bufferTime = 0;
                    if (*(_timeBuffers.begin()) > TIME_BACK_SHIFT_MS) {
                        bufferTime = *(_timeBuffers.begin()) - TIME_BACK_SHIFT_MS;
                    }
                    else {
                        bufferTime = 0;
                    }
                    SyncPoint syncPoint;
                    syncPoint.coord = res;
                    syncPoint.time = bufferTime;
                    _syncPoints.push(syncPoint);
                    if (!_filename.empty()) {
                        writeSyncPoint(syncPoint);
                    }
                    dropPoints();
                }
                _outputBuffers.push(buff);
                _buffers.erase(_buffers.begin(), (_buffers.begin() + WAVE_WIDTH_I));
                _timeBuffers.erase(_timeBuffers.begin(), (_timeBuffers.begin() + WAVE_WIDTH_I));
                _state = noExistsBaseLine;
                return;
            }
            else {
                return;
            }
        }
        }
    }
}

std::string WaveAnalyzer::getFilename() const
{
    return _filename;
}

void WaveAnalyzer::setFilename(const std::string& filename)
{
    _filename = filename;
}

bool WaveAnalyzer::findNegativeFront()
{
    bool res = false;
    int State = 1;
    int dy = 0;
    int w = 0;
    int i = 0;
    TWaveFront negatvieFront;
    negatvieFront.xl = 0;
    negatvieFront.xr = 0;
    negatvieFront.yl = 0;
    negatvieFront.yr = 0;
    SignalsBuffer::iterator it = _buffers.begin();
    while (it != _buffers.end()) {
        dy = *it - *(it + 1);
        if (std::abs(dy) < AvTOL_I) {
            w = 0;
        }
        else if (dy > 0) {
            w = 1;
        }
        else {
            w = 2;
        }
        switch (State) {
        case 1: {
            switch (w) {
            case 2:
                negatvieFront.xl = i;
                negatvieFront.yl = *it;
                State = 3;
                break;
            }
            break;
        }
        case 3: {
            switch (w) {
            case 0:
            case 1:
                negatvieFront.xr = i;
                negatvieFront.yr = *it;
                State = 1;
                if (checkFront(negatvieFront)) {
                    int dist = std::distance(_buffers.begin(), it);
                    _buffers.erase(_buffers.begin(), (_buffers.begin() + dist));
                    _timeBuffers.erase(_timeBuffers.begin(), (_timeBuffers.begin() + dist));
                    res = true;
                    return res;
                }
                break;
            }
        }
        }
        ++it;
        ++i;
    }
    return res;
}

bool WaveAnalyzer::decodeBuffer(const WaveAnalyzer::SignalsBuffer& buffer, unsigned int& result)
{
    unsigned int acode = 0;
    int startpoint = 0;
    int wavesize = buffer.size();
    const WaveAnalyzer::SignalsBuffer& wave = buffer;

    int i = 0;
    int k = 0;
    int dy = 0;
    int xref = 0;
    int FrontsSize = 0;
    int CodeLENGTH = 270;
    std::array<TWaveFront, 128> Fronts;
    TWaveFront emptyFront;
    emptyFront.xl = 0;
    emptyFront.xr = 0;
    emptyFront.yl = 0;
    emptyFront.yr = 0;
    std::fill(Fronts.begin(), Fronts.end(), emptyFront);

    bool Result = false;
    i = startpoint;
    int w = 0;
    int State = 1;

    while (i < (startpoint + CodeLENGTH)) {
        assert(k < Fronts.size());
        dy = wave.at(i) - wave.at(i + 1);
        if (std::abs(dy) < STOL_I) {
            w = 0;
        }
        else if (dy > 0) {
            w = 1;
        }
        else {
            w = 2;
        }

        switch (State) {
        case 1: {
            switch (w) {
            case 1: {
                assert(k < Fronts.size());
                Fronts.at(k).xl = i;
                Fronts.at(k).yl = wave.at(i);
                State = 2;
                break;
            }
            case 2: {
                assert(k < Fronts.size());
                Fronts.at(k).xl = i;
                Fronts.at(k).yl = wave.at(i);
                State = 3;
                break;
            }
            }
            break;
        }
        case 2: {
            switch (w) {
            case 0: {
                assert(k < Fronts.size());
                Fronts.at(k).xr = i;
                Fronts.at(k).yr = wave.at(i);
                if (checkFront(Fronts.at(k))) {
                    k++;
                    Fronts.at(k).xl = i;
                    Fronts.at(k).yl = wave.at(i);
                }
                State = 1;
                break;
            }
            case 2: {
                assert(k < Fronts.size());
                Fronts.at(k).xr = i;
                Fronts.at(k).yr = wave.at(i);
                if (checkFront(Fronts.at(k))) {
                    k++;
                    assert(k < Fronts.size());
                    Fronts.at(k).xl = i;           //
                    Fronts.at(k).yl = wave.at(i);  //
                    State = 3;
                }
                else {
                    State = 1;
                }
                break;
            }
            }
            break;
        }
        case 3: {
            switch (w) {
            case 0: {
                assert(k < Fronts.size());
                Fronts.at(k).xr = i;
                Fronts.at(k).yr = wave.at(i);
                if (checkFront(Fronts.at(k))) {
                    k++;
                    Fronts[k + 1].xl = i;           //
                    Fronts[k + 1].yl = wave.at(i);  //
                }
                State = 1;
                break;
            }
            case 1: {
                assert(k < Fronts.size());
                Fronts.at(k).xr = i;
                Fronts.at(k).yr = wave.at(i);
                if (checkFront(Fronts.at(k))) {
                    k++;
                    assert(k < Fronts.size());
                    Fronts.at(k).xl = i;
                    Fronts.at(k).yl = wave.at(i);
                    State = 2;
                }
                else {
                    State = 1;
                }
                break;
            }
            }
            break;
        }
        }
        i++;
    }

    FrontsSize = k;
    // assert(k < Fronts.size());
    if (k >= Fronts.size()) return Result;
    xref = 0;
    acode = 0;
    i = 0;
    for (int m = 0; m < FrontsSize; ++m) {
        assert(m < Fronts.size());
        if (((Fronts.at(m).xl + Fronts.at(m).xr) / 2) >= (xref + 6)) {
            acode = acode >> 1;
            if (Fronts.at(m).yl < Fronts.at(m).yr) {
                acode = acode | 0x80000000;
            }
            xref = (Fronts.at(m).xl + Fronts.at(m).xr) / 2;
            ++i;
            if (i >= 32) break;
        }
    }
    if (i != 32) {
        Result = false;
        result = 0;
        std::cout << "Unable to decode! fronts:" << i << std::endl;
    }
    else {
        Result = true;
        result = acode;
    }
    return Result;
}

bool WaveAnalyzer::checkFront(const WaveAnalyzer::TWaveFront& front)
{
    bool res = false;
    if (abs(front.yr - front.yl) >= AFTOL_I) {
        if ((front.xr - front.xl) <= DFTOL_I) {
            res = true;
        }
    }
    return res;
}

bool WaveAnalyzer::findBaseline()
{
    bool res = false;
    bool baseLine = false;
    int j = 0;
    SignalsBuffer::iterator it = _buffers.begin();
    while (j < (_buffers.size() - BASELINE_WINDOW_WIDTH)) {
        for (int i = 0; i < BASELINE_WINDOW_WIDTH; ++i, ++it) {
            if (*(it) >= *(it + 1) - ATOL_I) {
                baseLine = true;
            }
            else {
                baseLine = false;
                break;
            }
        }
        if (baseLine) {
            res = true;
            int dist = std::distance(_buffers.begin(), it);
            _buffers.erase(_buffers.begin(), (_buffers.begin() + dist));
            _timeBuffers.erase(_timeBuffers.begin(), (_timeBuffers.begin() + dist));
            break;
        }
        j += j + BASELINE_WINDOW_WIDTH;
    }
    return res;
}


WaveAnalyzer::WaveAnalyzer()
    : _outputFile("TEMPSAMPLES.bin")

{
    //_outputFile.open(QIODevice::WriteOnly);
    _state = noExistsBaseLine;
}

void WaveAnalyzer::addBufferWithTimecode(QSharedPointer<std::vector<signed short>> samples, GstClockTime timestamp, GstClockTime duration)
{
    const std::vector<signed short>& currentBuffer = samples.operator*();
    Q_ASSERT(!currentBuffer.empty());
    _buffers.insert(_buffers.end(), currentBuffer.begin(), currentBuffer.end());
    unsigned long size = samples->size();
    GstClockTime timePerSample = duration / size;

    for (unsigned long i = 0; i < size; ++i) {
        _timeBuffers.push_back(timestamp + i * timePerSample);
    }
}

bool WaveAnalyzer::getNextBuffer(WaveAnalyzer::SignalsBuffer& output)
{
    if (!_outputBuffers.empty()) {
        output = _outputBuffers.front();
        _outputBuffers.pop();
        return true;
    }
    return false;
}

bool WaveAnalyzer::getNextCoord(unsigned int& coord, GstClockTime& time)
{
    if (!_syncPoints.empty()) {
        coord = _syncPoints.front().coord;
        time = _syncPoints.front().time;
        _syncPoints.pop();

        return true;
    }
    return false;
}

bool WaveAnalyzer::getEnoughData()
{
    return _buffers.size() >= SAMPLE_SIZE_I;
}

void WaveAnalyzer::dropPoints()
{
    if (_syncPoints.size() > 5) {
        _syncPoints.pop();
    }
}

void WaveAnalyzer::writeSyncPoint(WaveAnalyzer::SyncPoint point)
{
    if (!_filename.empty()) {
        std::ofstream output(_filename + ".txt", std::ofstream::out | std::ofstream::app);
        if (output.is_open()) {
            output << point.time << " " << point.coord << "\n";
            output.close();
        }
    }
}
