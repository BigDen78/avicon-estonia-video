#include "splashscreen.h"
#include "ui_splashscreen.h"

SplashScreen::SplashScreen(QWidget* parent)
    : QWidget(parent)
    , ui(new Ui::SplashScreen)
{
    ui->setupUi(this);
    Qt::WindowFlags flags = windowFlags();
    this->setWindowFlags(flags | Qt::FramelessWindowHint | Qt::CustomizeWindowHint | Qt::WindowStaysOnTopHint);
}

SplashScreen::~SplashScreen()
{
    delete ui;
}

void SplashScreen::onProgress(int progress)
{
    ui->progressBar->setValue(progress);
    if (progress == 100) {
        hide();
    }
    else {
        if (!isVisible()) {
            Qt::WindowFlags flags = windowFlags();
            this->setWindowFlags(flags | Qt::FramelessWindowHint | Qt::CustomizeWindowHint | Qt::WindowStaysOnTopHint);
            show();
        }
    }
    repaint();
}
