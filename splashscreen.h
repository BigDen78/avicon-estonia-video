#ifndef SPLASHSCREEN_H
#define SPLASHSCREEN_H

#include <QWidget>

namespace Ui {
class SplashScreen;
}

class SplashScreen : public QWidget
{
    Q_OBJECT

public:
    explicit SplashScreen(QWidget *parent = nullptr);
    ~SplashScreen();

public slots:
    void onProgress(int progress);

private:
    Ui::SplashScreen *ui;
};

#endif // SPLASHSCREEN_H
