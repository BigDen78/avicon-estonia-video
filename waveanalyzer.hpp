#ifndef WAVEANALYZER_HPP
#define WAVEANALYZER_HPP

#include <gst/gstbuffer.h>
#include <vector>
#include <queue>
#include <array>
#include <QSharedPointer>
#include <QFile>

const static int BASELINE_WINDOW_WIDTH = 6;
const static int WAVE_WIDTH_I = 280;  // Wave Length
const static unsigned int SAMPLE_SIZE_I = 320;

const static unsigned int STOL_I = 700;    // Slope Tolerance
const static unsigned int ATOL_I = 400;    // Baseline Amplitude Tolerance
const static unsigned int AvTOL_I = 2000;  // Amplitude Tolerance
const static unsigned int AFTOL_I = 32767 / 6;
const static unsigned int DFTOL_I = 6;  // Front Max Length

const static unsigned int TIME_BACK_SHIFT_MS = 32000000;

class WaveAnalyzer
{
    using SampleType = signed short;
    using SignalsBuffer = std::vector<WaveAnalyzer::SampleType>;

    enum DataState
    {
        noExistsBaseLine,
        noNegFront,
        decodeWave,
    } _state;

    struct TWaveFront
    {
        int xl;
        int xr;
        int yl;
        int yr;
    };

    struct SyncPoint
    {
        unsigned int coord;
        GstClockTime time;
    };

private:
    SignalsBuffer _buffers;
    std::vector<GstClockTime> _timeBuffers;
    std::queue<SignalsBuffer> _outputBuffers;
    std::queue<SyncPoint> _syncPoints;
    std::string _filename;
    QFile _outputFile;


private:
    bool decodeBuffer(const SignalsBuffer& buffer, unsigned int& result);
    bool checkFront(const TWaveFront& front);
    bool findBaseline();
    bool findNegativeFront();

public:
    WaveAnalyzer();
    void addBufferWithTimecode(QSharedPointer<std::vector<signed short>> samples, GstClockTime timestamp, GstClockTime duration);
    void analyze();
    bool getNextBuffer(SignalsBuffer& output);
    bool getNextCoord(unsigned int& coord, GstClockTime& time);
    bool getEnoughData();
    void dropPoints();
    void writeSyncPoint(SyncPoint point);
    std::string getFilename() const;
    void setFilename(const std::string& filename);
};

#endif  // WAVEANALYZER_HPP
