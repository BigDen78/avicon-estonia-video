#include "hikvisioncameraconfigurationmanager.h"
#include "HCNetSDK.h"

HikvisionCameraConfigurationManager::HikvisionCameraConfigurationManager() {}

bool HikvisionCameraConfigurationManager::setCurrentTimeToCamera(const GstreamerThreadWorker::CameraType& cameraType)
{
    qDebug("HikvisionCameraConfigurationManager start");
    NET_DVR_INIT_CFG_ABILITY cngAbility;
    cngAbility.enumMaxLoginUsersNum = INIT_CFG_NUM_2048;
    cngAbility.enumMaxAlarmNum = INIT_CFG_NUM_2048;
    if (NET_DVR_SetSDKInitCfg(NET_SDK_INIT_CFG_ABILITY, &cngAbility)) {
        if (NET_DVR_Init()) {
            NET_DVR_SetConnectTime(1000, 1);
        }
        else {
            qDebug("HikvisionCameraConfigurationManager : NET_DVR_Init error, %d\n", NET_DVR_GetLastError());
        }
    }
    else {
        qDebug("HikvisionCameraConfigurationManager: NET_DVR_SetSDKInitCfg error, %d\n", NET_DVR_GetLastError());
    }
    LONG lUserID;
    NET_DVR_USER_LOGIN_INFO struLoginInfo = {0};
    struLoginInfo.bUseAsynLogin = 0;
    struLoginInfo.wPort = 8000;
    if (cameraType == GstreamerThreadWorker::CameraType::eCameraLeft) {
        strcpy(struLoginInfo.sDeviceAddress, "172.168.100.8");
        strcpy(struLoginInfo.sUserName, "admin");
        strcpy(struLoginInfo.sPassword, "5678efgh");
    }
    else if (cameraType == GstreamerThreadWorker::CameraType::eCameraRight) {
        strcpy(struLoginInfo.sDeviceAddress, "172.168.100.9");
        strcpy(struLoginInfo.sUserName, "admin");
        strcpy(struLoginInfo.sPassword, "1234abcd");
    }
    NET_DVR_DEVICEINFO_V40 struDeviceInfoV40 = {0};
    lUserID = NET_DVR_Login_V40(&struLoginInfo, &struDeviceInfoV40);
    if (lUserID < 0) {
        printf("HikvisionCameraConfigurationManager : Login error, %d\n", NET_DVR_GetLastError());
        NET_DVR_Cleanup();
        return false;
    }

    bool iRet = true;
    DWORD dwReturnLen;
    NET_DVR_TIME struParams = {0};

    iRet = NET_DVR_GetDVRConfig(lUserID, NET_DVR_GET_TIMECFG, 0xFFFFFFFF, &struParams, sizeof(NET_DVR_TIME), &dwReturnLen);

    if (!iRet) {
        printf("HikvisionCameraConfigurationManager : Config error, %d\n", NET_DVR_GetLastError());
        NET_DVR_Logout(lUserID);
        NET_DVR_Cleanup();
        return false;
    }

    time_t timer;
    if (time(&timer) == -1) {
        qDebug("Timer: Can not retrieve the calendar time.");
        return false;
    }
    else {
        struct tm* timeinfo;

        time(&timer);
        timeinfo = localtime(&timer);

        struParams.dwDay = static_cast<DWORD>(timeinfo->tm_mday);
        struParams.dwHour = static_cast<DWORD>(timeinfo->tm_hour);
        struParams.dwYear = static_cast<DWORD>(timeinfo->tm_year + 1900);
        struParams.dwMonth = static_cast<DWORD>(timeinfo->tm_mon + 1);
        struParams.dwMinute = static_cast<DWORD>(timeinfo->tm_min);
        struParams.dwSecond = static_cast<DWORD>(timeinfo->tm_sec);
    }

    iRet = NET_DVR_SetDVRConfig(lUserID, NET_DVR_SET_TIMECFG, 0xFFFFFFFF, &struParams, dwReturnLen);

    if (iRet) {
        qDebug("HikvisionCameraConfigurationManager : Time setting passed");
    }
    else {
        qDebug("HikvisionCameraConfigurationManager : Time setting error, %d\n", NET_DVR_GetLastError());
    }
    NET_DVR_Logout(lUserID);
    NET_DVR_Cleanup();

    return true;
}
